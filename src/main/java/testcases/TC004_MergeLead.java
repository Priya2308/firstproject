package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC004_MergeLead extends ProjectMethods{
;
	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription ="Creating a Lead";
		testNodes = "Leads";
		authors ="Priya";
		category = "smoke";
		dataSheetName="TC003";
		/*String cname="Wipro";
		String fname = "Priya";
		String lname = "M";*/
		
	}
	@Test(dataProvider="fetchData")
	public void loginLogout(String uname, String pwd, String ID, String fromId) throws Exception {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.click_CRMSFA()
		.clickLeads()
		.clickFindLeads2()
		.click_MergeLead()
		.click_fromLead()
		.enterFromLeadId(fromId)
		.clickFindFromLeads();
		/*.enter_LeadID(ID)
		.deleteLead();*/
		
	}

}












package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC002_CreateLead";
		testDescription ="Creating a Lead";
		testNodes = "Leads";
		authors ="Priya";
		category = "smoke";
		dataSheetName="TC002";
		/*String cname="Wipro";
		String fname = "Priya";
		String lname = "M";*/
		
	}
	@Test(dataProvider="fetchData")
	public void createLead(String uname, String pwd, String cname, String fname, String lname) {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		//.clickLogout();
		.click_CRMSFA()
		.clickLeads1()
		.click_CreateLead()
		.Enter_CompName(cname)
		.Enter_firstName(fname)
		.enter_lastName(lname)
		.click_CreateLeadButton()
		.viewLead();
		
	}

}












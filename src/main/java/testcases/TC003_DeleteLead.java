package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC003_DeleteLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC003_DeleteLead";
		testDescription ="Deleting a Lead";
		testNodes = "Leads";
		authors ="Priya";
		category = "smoke";
		dataSheetName="TC003";
				
	}
	@Test(dataProvider="fetchData")
	public void deleteLead(String uname, String pwd, String ID) {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin()
		.click_CRMSFA()
		.clickLeads()
		.clickFindLeads()
		.enter_LeadID(ID)
		.clickFindLeads()
		.clickFirstEntry()
		.deleteLead();
		
	}

}












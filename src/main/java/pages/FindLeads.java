package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeads extends ProjectMethods{

public FindLeads() {
	PageFactory.initElements(driver, this);
}

@FindBy(how=How.LINK_TEXT,using="Find Leads") WebElement findLeads;
@FindBy(how=How.XPATH, using ="//div[@class='subSectionBlock']//input") WebElement leadID;

public FindLeads clickFindLeads() {
	click(findLeads);
	return this;
}

public MergeLead clickFindLeads2()  {
	click(findLeads);
	return new MergeLead();
}

public DeleteLead enter_LeadID(String ID) {
	type(leadID, ID);
	return new DeleteLead();
}
}

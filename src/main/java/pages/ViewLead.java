package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class ViewLead extends ProjectMethods{

	public ViewLead() {
		PageFactory.initElements(driver,this);
	}
	
	@FindBy(how=How.LINK_TEXT, using = "Delete") WebElement delete;

	public ViewLead viewLead() {
		driver.quit();
		return this;
		
	}
	
	public ViewLead deleteLead() {
		click(delete);
		return this;
	}
}



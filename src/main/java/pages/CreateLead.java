package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using = "Create Lead") WebElement createLead;
	@FindBy(how=How.ID, using = "createLeadForm_companyName") WebElement comp_name;
	@FindBy(how=How.ID, using = "createLeadForm_firstName") WebElement fname;
	@FindBy(how=How.ID, using = "createLeadForm_lastName") WebElement lname;
	@FindBy(how=How.CLASS_NAME, using = "smallSubmit") WebElement cLead;
	
	public CreateLead click_CreateLead() {
		click(createLead);
		return this;
			}
	
	
	public CreateLead Enter_CompName(String cname) {
		type(comp_name, cname);
		return this;
	}
	
    public CreateLead Enter_firstName(String f_name) {
    	type(fname, f_name);
    	return this;
    }
    
    public CreateLead enter_lastName(String l_name) {
         type(lname, l_name);	
         return this;
    }
    
    public ViewLead click_CreateLeadButton() {
		click(cLead);
		return new ViewLead();
			}
}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyHome extends ProjectMethods{

	public MyHome() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Leads") WebElement leads;
	
	public CreateLead clickLeads1() {
		click(leads);
		return new CreateLead();
	}
	
	public FindLeads clickLeads() {
		click(leads);
		return new FindLeads();
	}
}

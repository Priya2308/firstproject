package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLead extends ProjectMethods{

	public MergeLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using = "Merge Leads") WebElement mergeLead;
	@FindBy(how=How.XPATH, using = "//table[@class='twoColumnForm']//img") WebElement from_lead;
	@FindBy(how=How.ID, using = "createLeadForm_firstName") WebElement fname;
	@FindBy(how=How.ID, using = "createLeadForm_lastName") WebElement lname;
	@FindBy(how=How.CLASS_NAME, using = "smallSubmit") WebElement cLead;
	
	public MergeLead click_MergeLead() {
		click(mergeLead);
		return this;
			}
	
	public MergeFromLead click_fromLead() {
		click(from_lead);
		return new  MergeFromLead();
	}
	
	
	}

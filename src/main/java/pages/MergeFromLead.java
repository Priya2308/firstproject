package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeFromLead extends ProjectMethods{

	public MergeFromLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.LINK_TEXT, using = "Merge Leads") WebElement mergeLead;
	@FindBy(how=How.XPATH, using = "//table[@class='twoColumnForm']//img") WebElement from_lead;
	@FindBy(how=How.XPATH, using = "//div[@class='x-panel-mc']//input") WebElement fromLeadId;
	@FindBy(how=How.XPATH, using = "//td[@class='x-panel-btn-td']//button") WebElement clickfindFromLead;
	@FindBy(how=How.CLASS_NAME, using = "smallSubmit") WebElement cLead;
	
	public MergeFromLead click_MergeLead() {
				click(mergeLead);
		return this;
			}
	
	public MergeFromLead click_fromLead() {
		click(from_lead);
		switchToWindow(1);
		return this;
	}
	
	public MergeFromLead enterFromLeadId(String fromId) {
		type(fromLeadId, fromId );
		return this;
	}
	
	public MergeFromLead clickFindFromLeads() {
		click(clickfindFromLead);
		return this;
	}
	
	}

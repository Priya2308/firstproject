package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class DeleteLead extends ProjectMethods{

	public DeleteLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.CLASS_NAME, using="subMenuButtonDangerous") WebElement delete;
	@FindBy(how=How.XPATH,using="//button[text()='Find Leads']") WebElement findLeads;
	@FindBy(how=How.XPATH,using="//table[@class='x-grid3-row-table']//a") WebElement click;
	
	
	public DeleteLead clickFindLeads() {
		click(findLeads);
		return this;
	}
	
	public DeleteLead clickFirstEntry() {
		click(click);
		return this;
	}
	
		
	public DeleteLead deleteLead() {
		click(delete);
		return this;
	}
	
}
